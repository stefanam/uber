package uber.stefana.com.uber;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangeUserDetails extends AppCompatActivity {

    @BindView(R.id.change_email)
    TextView email;
    @BindView(R.id.send_reset_email)
    Button sendResetEmaily;
    @BindView(R.id.change_car_type)
    EditText carType;
    @BindView(R.id.change_price_per_km)
    EditText pricePerKm;
    @BindView(R.id.change_login_progress)
    ProgressBar progressBar;
    @BindView(R.id.save)
    Button save;
    String price;
    UserDetails userDetails;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_user_details);
        ButterKnife.bind(this);

        email.setText(FirebaseAuth.getInstance().getCurrentUser().getEmail());
        sendResetEmaily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().sendPasswordResetEmail(email.getText().toString())
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(ChangeUserDetails.this, "Email sent", Toast.LENGTH_SHORT).show();
                                    FirebaseAuth.getInstance().signOut();
                                    Intent login = new Intent(ChangeUserDetails.this, LoginActivity.class);
                                    startActivity(login);
                                    finish();
                                }
                            }
                        });
            }
        });

        FirebaseDatabase.getInstance()
                .getReference("user-details")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        userDetails = dataSnapshot.getValue(UserDetails.class);
                        //user details ne sme biti null (na registraciji se pravi)
                        if (!TextUtils.isEmpty(userDetails.getCar())) {
                            findViewById(R.id.change_driver_info).setVisibility(View.VISIBLE);
                            save.setVisibility(View.VISIBLE);
                            //carType.setVisibility(View.VISIBLE);
                            carType.setText(userDetails.getCar());
                            //pricePerKm.setVisibility(View.VISIBLE);
                            price = String.valueOf(userDetails.getPricePerKmUSD());
                            pricePerKm.setText(price);
                        }else{

                            save.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(carType.getText().toString())) {
                    userDetails.setCar(carType.getText().toString());
                }

                if (!TextUtils.isEmpty(pricePerKm.getText().toString())) {
                    userDetails.setPricePerKmUSD(Double.parseDouble(pricePerKm.getText().toString()));
                }
                //slicno kao dohvatanje
                FirebaseDatabase.getInstance().getReference("user-details").child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .setValue(userDetails);
                Toast.makeText(ChangeUserDetails.this, "Changes saved", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(ChangeUserDetails.this, MainActivity.class);
                startActivity(i);

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();


    }
}
