package uber.stefana.com.uber;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class DriverChooserAdapter extends RecyclerView.Adapter<DriverChooserViewHolder>{

    private List<UserDetails> data = new ArrayList<>();
    private Callback callback;

    public DriverChooserAdapter(Callback callback) {
        this.callback = callback;
    }

    @NonNull
    @Override
    public DriverChooserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DriverChooserViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.driver_info_holder_layout, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(@NonNull DriverChooserViewHolder holder, int position) {
        final UserDetails item = data.get(position);
        holder.item = item;
        holder.driver.setText(item.getCar());
        holder.price.setText("Cena po km: \n " + item.getPricePerKmUSD() + " RSD");
        holder.distance.setText(String.format("Distanca:\n %.2f km", item.getDistance()));

        GlideApp.with(holder.itemView.getContext())
                .load(FirebaseStorage.getInstance().getReference(item.getImagePath()))
                .into(holder.driverImage);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<UserDetails> userDetails) {
        this.data = userDetails;
        notifyDataSetChanged();
    }

    public interface Callback {
        //To je metoda preko koje dobijamo informacije iz aktivitija,
        // jer nemamo referencu na aktiviti
        // a trebaju nam ti podaci za adapter
        String getAddress();
        LatLng getLatLng();
    }
}
