package uber.stefana.com.uber;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.widget.Toast;

import java.util.ArrayList;

public class PermissionHelper {

    private static final int PERMISSIONS_REQUEST_CODE = 123789;
    private Activity context;
    private PermissionObject[] permissions;
    private OnGrantedCallback callback;
    private int resString;

    public interface OnGrantedCallback {
        void permissionsGranted();
    }

    public PermissionHelper withActivity(Activity context) {
        this.context = context;
        return this;
    }

    public PermissionHelper askFor(PermissionObject... permissions) {
        this.permissions = permissions;
        return this;
    }

    public PermissionHelper withMessage(int resString) {
        this.resString = resString;
        return this;
    }

    public PermissionHelper withGrantedCallback(OnGrantedCallback callback) {
        this.callback = callback;
        return this;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean requestAll() {
        if (context == null) {
            return false;
        }
        final ArrayList<String> notGrantedPermissions = new ArrayList<>();
        ArrayList<String> importantPermissions = new ArrayList<>();
        for (int i = 0; i < permissions.length; i++) {
            PermissionObject permissionObject = permissions[i];
            if (!isGranted(permissionObject.getPermission())) {
                notGrantedPermissions.add(permissionObject.getPermission());
                if (isImportantPermission(permissionObject.getPermission())) {
                    importantPermissions.add(context.getString(permissionObject.getNameRes()));
                }
            }
        }
        if (notGrantedPermissions.size() > 0) {
            if (importantPermissions.size() > 0) {
                new AlertDialog.Builder(context)
                        .setMessage(context.getString(resString) + TextUtils.join(",", importantPermissions))
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                context.requestPermissions(notGrantedPermissions.toArray(new String[]{}), PERMISSIONS_REQUEST_CODE);
                            }
                        })
                        .create()
                        .show();
                return false;
            }
            context.requestPermissions(notGrantedPermissions.toArray(new String[]{}), PERMISSIONS_REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }

    public boolean arePermissionsGranted() {
        boolean granted = true;
        for (int i = 0; i < permissions.length; i++) {
            PermissionObject permissionObject = permissions[i];
            granted = granted && isGranted(permissionObject.getPermission());
        }
        return granted;
    }

    private boolean isImportantPermission(String permission) {
        return ActivityCompat.shouldShowRequestPermissionRationale(context, permission);
    }

    private boolean isGranted(String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public void disconnect() {
        this.context = null;
    }

    public void handleGrantResults(int code, String[] permissions, int[] grantResults) {
        if (code == PERMISSIONS_REQUEST_CODE) {
            boolean isDenied = false;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    isDenied = true;
                }
            }
            if (isDenied) {
                Toast.makeText(context, R.string.perm_denied, Toast.LENGTH_LONG).show();
            } else if (grantResults.length > 0) {
                if (callback != null) {
                    callback.permissionsGranted();
                }
            }
        }
    }
}
