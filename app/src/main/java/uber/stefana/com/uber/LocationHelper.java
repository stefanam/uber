package uber.stefana.com.uber;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class LocationHelper {

    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    private GoogleApiClient googleApiClient;
    private GoogleApiClient.ConnectionCallbacks callbacks;
    private GoogleApiClient.OnConnectionFailedListener failedCallbacks;
    private LocationRequest locationRequest;
    private LocationRequest DEFAULT_LOCATION_REQUEST;
    private LocationListener locationListener;
    private Location location;
    private PermissionHelper permissionHelper;
    private OnLocationCallback locationFoundCallback;

    public interface OnLocationCallback {
        void locationFound(Location location);
    }

    public LocationHelper(Activity activity) {
        permissionHelper = new PermissionHelper();
        permissionHelper.withActivity(activity)
                .askFor(
                        new PermissionObject(Manifest.permission.ACCESS_COARSE_LOCATION).withName(R.string.coarse_location),
                        new PermissionObject(Manifest.permission.ACCESS_FINE_LOCATION).withName(R.string.coarse_location)
                )
                .withGrantedCallback(new PermissionHelper.OnGrantedCallback() {
                    @Override
                    public void permissionsGranted() {
                        requestLocationUpdate();
                    }
                });
        DEFAULT_LOCATION_REQUEST = new LocationRequest();
        DEFAULT_LOCATION_REQUEST.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        DEFAULT_LOCATION_REQUEST.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        DEFAULT_LOCATION_REQUEST.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest = DEFAULT_LOCATION_REQUEST;
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location1) {
                if (location1 != null && googleApiClient.isConnected()) {
                    LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, locationListener);
                    if (locationFoundCallback != null) {
                        LocationHelper.this.location = location1;
                        locationFoundCallback.locationFound(location1);
                    }
                }
            }
        };
        callbacks = new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {
                if (permissionHelper.arePermissionsGranted()) {
                    requestLocationUpdate();
                } else {
                    permissionHelper.requestAll();
                }
            }

            @Override
            public void onConnectionSuspended(int i) {
            }
        };
        failedCallbacks = new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

            }
        };
        googleApiClient = new GoogleApiClient.Builder(activity.getApplicationContext())
                .addConnectionCallbacks(callbacks)
                .addOnConnectionFailedListener(failedCallbacks)
                .addApi(LocationServices.API)
                .build();
    }

    @SuppressLint("MissingPermission")
    private void requestLocationUpdate() {
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, locationListener);
        }
    }

    public void addLocationFoundCallback(OnLocationCallback locationFoundCallback) {
        this.locationFoundCallback = locationFoundCallback;
    }

    public void connect(Activity context) {
        if (!googleApiClient.isConnected()) {
            googleApiClient.connect();
        }
        permissionHelper.withActivity(context);
    }

    public void disconnect() {
        googleApiClient.disconnect();
        permissionHelper.disconnect();
        locationFoundCallback = null;
    }
}
