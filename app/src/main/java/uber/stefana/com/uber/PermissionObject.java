package uber.stefana.com.uber;

public class PermissionObject {

    private String permission;
    private int nameRes;

    public PermissionObject(String permission) {
        this.permission = permission;
    }

    public PermissionObject withName(int nameRes) {
        this.nameRes = nameRes;
        return this;
    }

    public int getNameRes() {
        return nameRes;
    }

    public String getPermission() {
        return permission;
    }
}
