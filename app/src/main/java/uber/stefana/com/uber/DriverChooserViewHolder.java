package uber.stefana.com.uber;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import agency.tango.android.avatarview.views.AvatarView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DriverChooserViewHolder extends RecyclerView.ViewHolder {


    @BindView(R.id.driver_id)
    public TextView driver;
    @BindView(R.id.distance)
    public TextView distance;
    @BindView(R.id.km_price)
    public TextView price;
    public UserDetails item;
    @BindView(R.id.driver_image)
    public AvatarView driverImage;

    public DriverChooserViewHolder(View itemView, final DriverChooserAdapter.Callback callback) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Drive drive = new Drive();
                drive.setAddress(callback.getAddress());
                drive.setCustomerId(FirebaseAuth.getInstance().getUid());
                drive.setDriverId(item.getDriverId());
                drive.setPeopleNumber(1);
                drive.setStatus("WAITING FOR DRIVER");
                drive.setTimeRemaning(Long.MAX_VALUE);
                FirebaseDatabase.getInstance().getReference("drives").setValue(drive);
            }
        });
    }
}
