package uber.stefana.com.uber;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.regex.Pattern;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    AutoCompleteTextView email;
    EditText password;
    Button buttonSignIn;
    Button buttonRegister;
    TextView iam;
    RadioGroup driverOrcustomer;
    RadioButton driver;
    RadioButton customer;
    private EditText carType;
    private EditText pricePerKm;
    ProgressBar progressBar;

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        progressBar = findViewById(R.id.login_progress);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        buttonSignIn = findViewById(R.id.email_sign_in_button);
        buttonRegister = findViewById(R.id.register);
        iam = findViewById(R.id.iam);
        driverOrcustomer = findViewById(R.id.driverOrCustomer);
        driver = findViewById(R.id.driver);
        customer = findViewById(R.id.customer);
        carType = findViewById(R.id.car_type);
        pricePerKm = findViewById(R.id.price_per_km);
        mAuth = FirebaseAuth.getInstance();


        buttonRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (driverOrcustomer.getVisibility() != View.VISIBLE) {
                    iam.setVisibility(View.VISIBLE);
                    driverOrcustomer.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    mAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                                       @Override
                                                       public void onComplete(@NonNull Task<AuthResult> task) {
                                                           if (task.isSuccessful()) {
                                                               //imamo usera
                                                               UserDetails userDetails = new UserDetails();
                                                               if (!TextUtils.isEmpty(carType.getText().toString()))
                                                                   userDetails.setCar(carType.getText().toString());
                                                               if (!TextUtils.isEmpty(pricePerKm.getText().toString()))
                                                                   userDetails.setPricePerKmUSD(Double.parseDouble(pricePerKm.getText().toString()));

                                                               DatabaseReference reference = FirebaseDatabase.getInstance().getReference("user-details");
                                                               reference.child(task.getResult().getUser().getUid())
                                                                       //ovde smestamo objekte u firebase
                                                                       .setValue(userDetails)
                                                                       .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                           @Override
                                                                           public void onComplete(@NonNull Task<Void> task) {
                                                                               if (task.isSuccessful()) {
                                                                                   // Sign in success, update UI with the signed-in user's information

                                                                                   Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                                                                   startActivity(i);
                                                                               } else {
                                                                                   Toast.makeText(LoginActivity.this, task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                                                               }
                                                                           }
                                                                       });
                                                           } else {
                                                               // If sign in fails, display a message to the user.
                                                               Toast.makeText(LoginActivity.this, task.getException().getLocalizedMessage(),
                                                                       Toast.LENGTH_SHORT).show();
                                                           }
                                                           progressBar.setVisibility(View.GONE);
                                                           if (customer.isChecked()){
                                                           carType.setText("");
                                                           pricePerKm.setText("0.00");
                                                               }
                                                       }
                                                   }
                            );
                }
            }
        });

        buttonSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (driverOrcustomer.getVisibility() == View.VISIBLE) {
                    driverOrcustomer.setVisibility(View.GONE);
                    iam.setVisibility(View.GONE);
                    findViewById(R.id.driver_info).setVisibility(View.GONE);
                } else {

                    if (!TextUtils.isEmpty(email.getText().toString()) && !TextUtils.isEmpty(password.getText().toString())) {

                        progressBar.setVisibility(View.VISIBLE);
                        FirebaseAuth.getInstance().signInWithEmailAndPassword(email.getText().toString(), password.getText().toString()).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(AuthResult authResult) {
                                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(i);
                                progressBar.setVisibility(View.GONE);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(LoginActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                cancelIfNotValid(email, password);
                                progressBar.setVisibility(View.GONE);
                            }

                        });
                    }
                }
            }
        });

        driver.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    findViewById(R.id.driver_info).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.driver_info).setVisibility(View.GONE);
                }
            }
        });

        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
        }
    }

    private boolean checkIfPassIsConfirmed(EditText password) {

        if (password.getText().toString().length() < 5) {
            Toast.makeText(LoginActivity.this, "Password doesn't match", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;

    }


    //ovo je metoda koja proverava mejl

    private boolean checkEmail(EditText email) {

        return EMAIL_ADDRESS_PATTERN.matcher(email.getText().toString()).matches();
    }


    //ove brise napisano ako nije validno

    private void cancelIfNotValid(EditText email, EditText firstPass) {

        email.setText("");
        firstPass.setText("");
    }


}

