package uber.stefana.com.uber;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.UUID;

import agency.tango.android.avatarview.views.AvatarView;

public class NavigationWrapper {

    private NavigationView navigationView;
    private AvatarView avatarView;
    private UserDetails userDetails;

    public void setNavigationView(final NavigationView navigationView) {
        this.navigationView = navigationView;
        avatarView = (AvatarView) navigationView.getHeaderView(0).findViewById(R.id.driver_image);

        //ovo setuje korisnikov email u headeru ispod slike
        View headerView = navigationView.getHeaderView(0);
        TextView navEmail = (TextView) headerView.findViewById(R.id.email_header);
        navEmail.setText(FirebaseAuth.getInstance().getCurrentUser().getEmail());

        //Ovo stavlja u header da pise da li je driver ili customer
        final TextView navStatus = (TextView) headerView.findViewById(R.id.status);
        FirebaseDatabase.getInstance()
                .getReference("user-details")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        userDetails = dataSnapshot.getValue(UserDetails.class);
                        //user details ne sme biti null (na registraciji se pravi)
                        if (!TextUtils.isEmpty(userDetails.getCar())) {
                            navStatus.setText("Driver");
                        } else {
                            navStatus.setText("Customer");
                        }

                        //ucitavamo sliku od avatara ako je ima na firebaseu
                        if (!TextUtils.isEmpty(userDetails.getImagePath())) {
                            StorageReference storage = FirebaseStorage.getInstance().getReference(userDetails.getImagePath());
                            GlideApp.with(navigationView.getContext())
                                    .load(storage)
                                    .into(avatarView);
                        } else {
                            GlideApp.with(navigationView.getContext())
                                    .load("https://cdn1.iconfinder.com/data/icons/user-pictures/100/female1-512.png")
                                    .into(avatarView);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

        avatarView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

    }

    public void setSelectedImage(Image selectedImage) {
        GlideApp.with(navigationView.getContext())
                .load(selectedImage.getPath())
                .into(avatarView);
        save(selectedImage);
    }

    public void save(Image selectedImage) {
        if (selectedImage != null) {
            //uploadovati sliku

            final String firebaseImagePath = "images/" + UUID.randomUUID().toString();

            final ProgressDialog progressDialog = new ProgressDialog(navigationView.getContext());
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.show();

            FirebaseStorage
                    .getInstance()
                    .getReference()
                    .child(firebaseImagePath)
                    .putFile(Uri.fromFile(new File(selectedImage.getPath())))
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();

                            //  imageLoader.loadImage(firebaseImagePath, avatarView, ImageType.GALLERY);
                            userDetails.setImagePath(firebaseImagePath);
                            saveUserDetails();

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(navigationView.getContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setProgress((int) progress);
                        }
                    });
        } else {
            //  saveNote(note);
        }
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Context context = navigationView.getContext();

        if (id == R.id.nav_logout) {
            FirebaseAuth.getInstance().signOut();
            Intent i = new Intent(context, LoginActivity.class);
            context.startActivity(i);
        } else if (id == R.id.nav_account_settings) {
            Intent intent = new Intent(context, ChangeUserDetails.class);
            context.startActivity(intent);
        }

        DrawerLayout drawer = ((View) navigationView.getParent()).findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showFileChooser() {
        ImagePicker.create((Activity) navigationView.getContext())
                .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .single() // single mode
                .start(); // start image picker activity with request code
    }

    private void saveUserDetails() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("user-details");
        reference.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                //ovde smestamo objekte u firebase
                .setValue(userDetails)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
    }
}
