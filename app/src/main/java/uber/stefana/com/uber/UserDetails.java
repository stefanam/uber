package uber.stefana.com.uber;

import android.media.Image;

import com.google.firebase.database.Exclude;

public class UserDetails {

    private String car;
    private double pricePerKmUSD;
    private double lat;
    private double lon;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    private String imagePath;

    @Exclude
    private double  distance;
    @Exclude
    private String driverId;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public double getPricePerKmUSD() {
        return pricePerKmUSD;
    }

    public void setPricePerKmUSD(double pricePerKmUSD) {
        this.pricePerKmUSD = pricePerKmUSD;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }


}
