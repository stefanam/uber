package uber.stefana.com.uber;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DriverChooserActivity extends AppCompatActivity implements DriverChooserAdapter.Callback {

    private static final String ADDRESS = "address";
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.list)
    RecyclerView recyclerView;

    private DriverChooserAdapter adapter;
    private double lat;
    private double lon;
    private String address;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_chooser);
        ButterKnife.bind(this);
        adapter = new DriverChooserAdapter(this);
        getSupportActionBar().setTitle("Choose driver from list");

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });

        refreshData();
    }

    private void refreshData() {
        swipeRefreshLayout.setRefreshing(true);

        //dohvatamo listu svih vozaca sa Firebase-a

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("user-details");

        Query query = reference.orderByChild("pricePerKmUSD").startAt(0.000000001);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                GenericTypeIndicator<Map<String, UserDetails>> t =
                        new GenericTypeIndicator<Map<String, UserDetails>>() {
                        };
                Map<String, UserDetails> value = dataSnapshot.getValue(t);
                ArrayList<UserDetails> drivers = new ArrayList<>();

                Iterator<Map.Entry<String, UserDetails>> iterator = value.entrySet().iterator();
                while (iterator.hasNext()){
                    Map.Entry<String, UserDetails> next = iterator.next();
                    UserDetails item = next.getValue();
                    if (next.getKey().equals(FirebaseAuth.getInstance().getUid()))continue;
                    item.setDriverId(next.getKey());
                    item.setDistance(MapUtils.CalculationByDistance(new LatLng(lat, lon), new LatLng(item.getLat(), item.getLon())));
                    drivers.add(item);
                }
                adapter.setData(drivers);
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        lat = this.getIntent().getExtras().getDouble("LAT");
        lon = this.getIntent().getExtras().getDouble("LON");
        address = getIntent().getStringExtra(ADDRESS);

    }

    public static void openDriverDetails(Context context, double latitude, double longitude, String address) {
        Intent intent = new Intent(context, DriverChooserActivity.class);
        intent.putExtra("LAT", latitude);
        intent.putExtra("LON", longitude);
        intent.putExtra(ADDRESS, address);
        context.startActivity(intent);
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public LatLng getLatLng() {
        return new LatLng(lat, lon);
    }
}
